FROM phpdockerio/php:8.2-cli

RUN apt-get update; \
    apt-get -y --no-install-recommends install \
        composer \
        php82-pdo; \
    apt-get clean; \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /usr/share/doc/*

# Create volume and default dir
RUN mkdir /volume
WORKDIR /volume
VOLUME /volume